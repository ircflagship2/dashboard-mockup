function initmap() {
    // set up the map
    map = L.map('map').setView([51.505, -0.09], 13);

    // create the tile layer with correct attribution
    var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var attrib='Map data © OpenStreetMap contributors | Contains Ordnance Survey data © Crown copyright and database right 2013';
    var osm = new L.TileLayer(osmUrl, {attribution: attrib});

    map.addLayer(osm);
}

function addGeoJson() {
    // add geojson shape
    // note, if running this locally (file:// protocol), you may want to open the html file in an insecure
    // browser, like "chrome --disable-web-security".

    function onEachFeature(feature, layer) {
        // does this feature have a property named popupContent?
        layer.bindPopup(feature.properties.NAME);

        layer.on('click', function() {
            console.log(feature);

            this.toggleStyle = !this.toggleStyle;
            if (this.toggleStyle) {
                this.setStyle({
                    "color": "#ff0000",
                    "weight": 2,
                    "opacity": 0.8,
                    "fillOpacity": 0.6
                });
            } else {
                this.setStyle({
                    "color": "#ff7800",
                    "weight": 1,
                    "opacity": 0.6,
                    "fillOpacity": 0.2
                });
            }
        });
    }

    var regionStyle = {
        "color": "#ff7800",
        "weight": 1,
        "opacity": 0.6,
        "fillOpacity": 0.2
    };

    $.getJSON("pages/ukregions/regions.json", function(data) {
        var shape = L.geoJson(data, {
            style: regionStyle,
            onEachFeature: onEachFeature
        }).addTo(map);
        map.fitBounds(shape.getBounds());
    });
}

$(function() {
    initmap();
    addGeoJson();
});