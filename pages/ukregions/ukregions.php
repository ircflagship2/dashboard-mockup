<?php
$glob->libs['leaflet']();
$glob->js[] = "pages/ukregions/ukregions.js";
$glob->css[] = "pages/ukregions/ukregions.css";
?>

<h3>UK Regions</h3>
<p>
    Data based on <a href="https://www.ordnancesurvey.co.uk/opendatadownload/products.html">Ordnance Survey</a> data (below: european_region_region).
    <ul>
        <li>Convert to geoJSON: <code>ogr2ogr -t_srs WGS84 -f GeoJSON regions.json bdline_gb/Data/european_region_region.shp</code></li>
        <li>Upload and simplify (below is 0.59%, at 1.52MB) using <a href="http://mapshaper.org">mapshaper.org</a></li>
        <li>Export as geoJSON.</li>
    </ul>
    Note that uploading the shp file to mapshaper causes problems. So convert it to geoJSON using ogr2ogr first.
    Use mapshaper instead of ogr2ogr's simply method, as it doesn't produce pretty borders between polygons, like mapshaper does.
</p>
<h3>Things to note</h3>
<p>
<ul>
    <li>Regions are clickable, but the data isn't the cleanest. Eevery little island in the Scottish region is an individual
    clickable polygon now. If this functionality is needed, I need to look into using data available in the geojson file
    more explicitely to combining polygons into regions</li>
    <li>Always remember to attribute Ordnance Survey, as required by their license.</li>
    <li>Currently using a free OSM map. We can easily switch to e.g. Cloudmade or Mapbox (free for (rather) low usage) if we want
    prettier, styled maps. That's also a good place to host maps, by the way.</li>
</ul>
</p>

<div id="map"></div>