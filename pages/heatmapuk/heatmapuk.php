<?php
require_once('services/shapemarkers/markers.php');


$glob->libs['leaflet']();
//$glob->js[] = "http://cdn.leafletjs.com/leaflet-0.4.4/leaflet-src.js";
$glob->js[] = "pages/heatmapuk/heatcanvas2.js";
$glob->js[] = "pages/heatmapuk/heatcanvas-leaflet2.js";
$glob->js[] = "pages/heatmapuk/heatmapuk.js";
?>
<div id="map" style="height:500px;" ></div>
<div id="status"></div>

<h3>Heatmap</h3>
<p>
    The final heatmap looks pretty good, but it's not exactly fast, nor are the animations that smooth.
    I doubt it's possible to do much about either.
</p>