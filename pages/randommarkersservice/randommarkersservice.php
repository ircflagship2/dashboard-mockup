<h3>About</h3>
The Shape Markers service will return random markers in UK. It's pretty slow if generating more than 1000 markers.
10000 takes a a handful seconds. Note that the markers lie within a very rough bounding polygon, so they may be placed in the
water upon closer inspection.

<h3>Request</h3>
<code><b>GET:</b> /services/shapemarkers?amount=$amountOfMarkersYouWant$</code><br/>

<h3>Response</h3>
<code><b>JSON, e.g.:</b> { coordinates : [ { lat: -0.1, lng: 54.2}, { lat: -0.15, lng: 54.5} ... ] }$</code><br/>

<h3>Example</h3>
<a href="services/shapemarkers/?amount=10">Generate 10 random coordinates</a>