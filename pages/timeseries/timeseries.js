extend = function(destination, source) {
    for (var property in source) {
        if (property in destination) {
            if ( typeof source[property] === "object" &&
                typeof destination[property] === "object") {
                destination[property] = extend(destination[property], source[property]);
            } else {
                continue;
            }
        } else {
            destination[property] = source[property];
        };
    }
    return destination;
};

// Wrapping in nv.addGraph allows for '0 timeout render', stores rendered charts in nv.graphs, and may do more in the future... it's NOT required
var chart;

var data = [];
d3.csv("pages/timeseries/us-flu-trends.txt")
    .row(function(d) {
        console.log(d.Date);
        var date = moment(d.Date, "YYYY-MM-DD").toDate();

        var i = 0;
        $.each(d, function(key, val) {
            if (key == "Date") return;
            if (key != "United States" && key != "California" && key != "Florida" && key != "New York") return;

            if (typeof data[i] == "undefined") {
                data[i] = {
                    key : key,
                    values: []
                };
            }

            data[i].values.push({x:date, y: parseInt(val)});

            i++; });
    }).get(function() {
        console.log(data);
        created3();
    });

function created3() {

    nv.addGraph(function() {
        chart = nv.models.lineWithFocusChart()
            .options({
                margin: {left: 100, bottom: 100},
                /*x: function(d,i) {
                    console.log(d);
                    return d
                },*/
                showXAxis: true,
                showYAxis: true,
                transitionDuration: 250
            })
           //.showLegend(false);
        ;

        // chart sub-models (ie. xAxis, yAxis, etc) when accessed directly, return themselves, not the parent chart, so need to chain separately
        /*chart.xAxis
            .axisLabel("Date")
            .tickFormat(d3.time.format("%Y"));*/

        chart.xAxis
            .tickFormat(function(d){ return new Date(d) });

        chart.yAxis

            .axisLabel('Flu level')
            .tickFormat(d3.format(',.2f'))
        ;
        chart.y2Axis

            .axisLabel('')
            .tickFormat(d3.format(',.2f'))
        ;
        //chart.showLabels(false);

        d3.select('#chart1 svg')
            .datum(data)
            .call(chart);

        //TODO: Figure out a good way to do this automatically
        nv.utils.windowResize(chart.update);
        //nv.utils.windowResize(function() { d3.select('#chart1 svg').call(chart) });

        chart.dispatch.on('stateChange', function(e) { nv.log('New State:', JSON.stringify(e)); });

        return chart;
    });


}
