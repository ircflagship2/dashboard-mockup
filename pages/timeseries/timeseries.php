<?php
require_once('services/shapemarkers/markers.php');

$glob->js[] = "//cdnjs.cloudflare.com/ajax/libs/crossfilter/1.3.1/crossfilter.min.js";
$glob->js[] = "pages/timeseries/novus-nvd3-d51729c/lib/d3.v3.js";
$glob->js[] = "pages/timeseries/novus-nvd3-d51729c/lib/d3.v3.js";
$glob->js[] = "pages/timeseries/novus-nvd3-d51729c/lib/crossfilter.js";
$glob->js[] = "pages/timeseries/novus-nvd3-d51729c/nv.d3.js";
$glob->js[] = "pages/timeseries/novus-nvd3-d51729c/src/tooltip.js";
$glob->js[] = "pages/timeseries/novus-nvd3-d51729c/src/utils.js";
//$glob->js[] = "pages/timeseries/novus-nvd3-d51729c/src/models/legend.js";
$glob->js[] = "pages/timeseries/novus-nvd3-d51729c/src/models/axis.js";
$glob->js[] = "pages/timeseries/novus-nvd3-d51729c/src/models/scatter.js";
$glob->js[] = "pages/timeseries/novus-nvd3-d51729c/src/models/line.js";
$glob->js[] = "pages/timeseries/novus-nvd3-d51729c/src/models/lineWithFocusChart.js";
$glob->js[] = "pages/timeseries/novus-nvd3-d51729c/examples/stream_layers.js";
$glob->js[] = "pages/timeseries/moment.js";
$glob->js[] = "pages/timeseries/timeseries.js";
$glob->css[] = "pages/timeseries/series.css";
$glob->css[] = "pages/timeseries/novus-nvd3-d51729c/nv.d3.css";

generateData();

function generateData() {
    //get US flu data
    $lines = file('pages/timeseries/us-flu-trends.txt');

    //first line is header
    $header = $lines[0];
    //remove header line from data
    array_shift($lines);

    foreach($lines as $line) {
        //var_dump($line);
    }
}

?>
<h1>Time Series with slider</h1>
<p>
An experiment with d3 and cross filter. Based on NVD3.org's reusable d3 charts library. If it's any good, it could
    prove to save some time to use that instead of "raw" d3, whenever plotting basic charts. Data shown is the US
    google flu trends.
</p>
<p>
    The ticks and x-axis labels are obviously off here, but that's easy to fix. I'm also not sure about the default
    slider style. I think it's counter-intuitive. It looks like the disabled parts are the activated parts to me.
</p>
<p>
    The amount of data shown here seems to be a bit too much. It takes a little too long to generate, and the browser
    doesn't respond instantly to updates to the chart.
</p>

<div id="chart1" >
    <svg style="height: 500px;"></svg>
</div>


<script>

</script>