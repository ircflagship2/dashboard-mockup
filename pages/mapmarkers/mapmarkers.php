<?php
$glob->libs['leaflet']();

$glob->js[] = "http://leaflet.github.io/Leaflet.markercluster/dist/leaflet.markercluster-src.js";
$glob->css[] = "http://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.css";
$glob->css[] = "http://leaflet.github.io/Leaflet.markercluster/dist/MarkerCluster.Default.css";
$glob->js[] = "http://leaflet.github.io/Leaflet.markercluster/example/realworld.388.js";

$glob->js[] = "pages/mapmarkers/mapmarkers.js";
$glob->css[] = "pages/mapmarkers/mapmarkers.css";

?>
<h3>
    Plotting huge amounts of Map Markers in leaflet.
</h3>
<p>
    <code>Status: <span id="status">Loading page...</span></code>
</p>
<div id="map"></div>

