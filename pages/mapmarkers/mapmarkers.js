var map;

function initmap() {
    // set up the map
    map = L.map('map').setView([51.505, -0.09], 7);

    // create the tile layer with correct attribution
    var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
    var attrib='Map data © OpenStreetMap contributors';
    var osm = new L.TileLayer(osmUrl, {attribution: attrib});
    map.addLayer(osm);
}

function addMarkers() {
    //get random UK markers
    var amount = 10000;
    $('#status').text("Fetching " + amount + " markers... (never mind how long this takes, this can be sped up considerably.)");

        $.getJSON('services/shapemarkers?amount=' + amount, function(data) {
            $('#status').text("Adding " + amount + "markers to map NOW (this is the crucial part - how slow is this on slow PCs?)!");
            //Add a short wait, so we make sure #status is updated.
            setTimeout(function(){
                var markers = L.markerClusterGroup();

                $.each(data.coordinates, function(key,coord) {
                    var title = "Tweet: I've got the flu :(";
                    var marker = L.marker(new L.LatLng(coord.lat, coord.lng), { title: title });
                    marker.bindPopup(title);
                    markers.addLayer(marker);
                });

                map.addLayer(markers);
            },50);
        });
}

$(function() {
    initmap();
    addMarkers();
});