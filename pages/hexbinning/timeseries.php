<?php
require_once('services/shapemarkers/markers.php');

$glob->js[] = "//cdnjs.cloudflare.com/ajax/libs/crossfilter/1.3.1/crossfilter.min.js";

generateData();

function generateData() {
    //get US flu data
    $lines = file('pages/timeseries/us-flu-trends-data-only.txt');

    //first col is date, so there's n-1 cities
    $amountCities = count(explode(",", $lines[0])) - 1;

    //get some random uk "cities"
    $generator = new RandomMarkerGenerator('services/shapemarkers/GBR.geo.json');
    $cityCoords = $generator->generate($amountCities);
    $cityCoords = json_decode($cityCoords, true);

    //var_dump($cityCoords);

    $i = 1;
    foreach($cityCoords['coordinates'] as $coordinate) {
        //var_dump($coordinate);
    }
}

?>
<h1>Time Series</h1>
<p>
    An experiment with d3 and cross filter. Based on NVD3.org's reusable d3 charts library. If it's any good, it could
    prove to save some time to use that instead of "raw" d3, whenever plotting basic charts. Data shown is the US
    google flu trends.
</p>

<div id="chart">
    <svg style="height: 500px;"></svg>
</div>

<script src="pages/timeseries/novus-nvd3-d51729c/lib/d3.v3.js"></script>
<script src="pages/timeseries/novus-nvd3-d51729c/lib/crossfilter.js"></script>
<script src="pages/timeseries/novus-nvd3-d51729c/nv.d3.js"></script>
<script src="pages/timeseries/novus-nvd3-d51729c/src/tooltip.js"></script>
<script src="pages/timeseries/novus-nvd3-d51729c/src/utils.js"></script>
<script src="pages/timeseries/novus-nvd3-d51729c/src/models/legend.js"></script>
<script src="pages/timeseries/novus-nvd3-d51729c/src/models/axis.js"></script>
<script src="pages/timeseries/novus-nvd3-d51729c/src/models/scatter.js"></script>
<script src="pages/timeseries/novus-nvd3-d51729c/src/models/line.js"></script>
<script src="pages/timeseries/novus-nvd3-d51729c/src/models/lineWithFocusChart.js"></script>
<script src="pages/timeseries/novus-nvd3-d51729c/examples/stream_layers.js"></script>
<script>
    extend = function(destination, source) {
        for (var property in source) {
            if (property in destination) {
                if ( typeof source[property] === "object" &&
                    typeof destination[property] === "object") {
                    destination[property] = extend(destination[property], source[property]);
                } else {
                    continue;
                }
            } else {
                destination[property] = source[property];
            };
        }
        return destination;
    };

    nv.addGraph(function() {
        var chart = nv.models.lineWithFocusChart();

        chart.xAxis
            .tickFormat(d3.format(',f'));
        chart.x2Axis
            .tickFormat(d3.format(',f'));

        chart.yAxis
            .tickFormat(d3.format(',.2f'));
        chart.y2Axis
            .tickFormat(d3.format(',.2f'));

        var dimension = testCrossfilterData().data;

        var data = normalizeData(dimension.top(Infinity),
            [
                {
                    name: 'Stream #1',
                    key: 'stream1'
                },
                {
                    name: 'Stream #2',
                    key: 'stream2'
                },
                {
                    name: 'Stream #3',
                    key: 'stream3'
                }
            ], 'x');

        d3.select('#chart svg')
            .datum(data)
            .transition().duration(500)
            .call(chart);

        nv.utils.windowResize(chart.update);

        return chart;
    });

    function normalizeData(data, series, xAxis)
    {
        var sort = crossfilter.quicksort.by(function(d) { return d[xAxis]; });
        var sorted = sort(data, 0, data.length);

        var result = [];

        series.forEach(function(serie, index)
        {
            result.push({key: serie.name, values: [], color: serie.color});
        });

        data.forEach(function(data, dataIndex)
        {
            series.forEach(function(serie, serieIndex)
            {
                result[serieIndex].values.push({x: data[xAxis],  y: data[serie.key]});
            });
        });

        return result;
    };

    function testCrossfilterData() {
        var data = crossfilter(testData());

        try
        {
            data.data = data.dimension(function(d) { return d.y; });
        } catch (e)
        {
            console.log(e.stack);
        }

        return data;
    }

    function testData() {

        var data1 = [];
        var data2 = [];
        var data3 = [];

        stream_layers(3,128,.1).map(function(layer, index) {
            layer.forEach(function(item, i) {
                var object = { x: item.x };
                object['stream' + (index + 1)] = item.y;
                eval('data' + (index + 1)).push(object);
            });
        });

        var data = extend(data1, data2);
        var result = extend(data, data3);

        return result;
    }
</script>