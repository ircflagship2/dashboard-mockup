<?php
$glob->libs['leaflet']();
$glob->js[] = "https://cdnjs.cloudflare.com/ajax/libs/d3/3.3.10/d3.min.js";
$glob->js[] = "http://d3js.org/d3.hexbin.v0.js";
$glob->js[] = "pages/hexbinning/colorbrewer.js";
$glob->js[] = "pages/hexbinning/leaflet.hexbin-layer.js";

$glob->css[] = "pages/hexbinning/hexbinning.css";
$glob->js[] = "pages/hexbinning/hexbinning.js";
?>

<h1>Hexbins</h1>
<p>
    Combines d3 hexbins and leaflet. The bins could be made prettier, but this should suffice as an example.
</p>

<div id='quake' data-source="pages/hexbinning/earthquakes.json"></div>