<?php if ($inc != "fj823qjva89jasjeou8vasjfoa") die("direct file access not allowed"); ?>
<?php


$glob = new stdClass();
$glob->css = array();
$glob->js = array();

$glob->libs = array();
$glob->libs['leaflet'] = function() {
    global $glob;
    $glob->js[] = "http://cdn.leafletjs.com/leaflet-0.7/leaflet.js";
    $glob->css[] = "http://cdn.leafletjs.com/leaflet-0.7/leaflet.css";
};

function isActiveMenu($page) {
    if ($_GET['page'] == $page || ($page == "front" && empty($_GET['page'])))
        echo "active";
}

function loadPage($page) {

    global $glob;

    if (empty($page))
        $page = "front";

    $pageParsed = preg_replace("/[^A-Za-z0-9 ]/", '', $page);

    $fileLocation = "pages/" . $pageParsed . "/" . $pageParsed . ".php";

    if ($pageParsed != $page || !file_exists($fileLocation)) {
        require('pages/errors/404.php');
    } else {
        require($fileLocation);
    }
}