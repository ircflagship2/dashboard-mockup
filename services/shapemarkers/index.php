<?php

require_once("markers.php");

//how many does user want?
if (isset($_GET['amount']))
    $amount = intval($_GET['amount']);
else
    $amount = 1;
if ($amount < 1) $amount = 1;
if ($amount > 100000) $amount = 100000;

$generator = new RandomMarkerGenerator('GBR.geo.json');
echo $generator->generate($amount);