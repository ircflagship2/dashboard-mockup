<?php

class RandomMarkerGenerator {

    private $geojson;

    public function __construct($filename) {
        $this->geojson = json_decode(file_get_contents($filename));
    }

    public function generate($amount) {

        $shapes = array();

        $minLat = 99999999;
        $minLng = 99999999;
        $maxLat = -99999999;
        $maxLng = -99999999;

        //var_dump($geojson);
        foreach($this->geojson->features as $key => $features) {
            foreach($features->geometry as $key => $geometry) {
                if ($key == "coordinates") {
                    foreach($geometry as $geomGroup) {
                        foreach($geomGroup as $coordinates) {
                            $shapes[] = $coordinates;
                            foreach($coordinates as $coord) {
                                if ($coord[0] < $minLat) $minLat = $coord[0];
                                if ($coord[0] > $maxLat) $maxLat = $coord[0];
                                if ($coord[1] < $minLng) $minLng = $coord[1];
                                if ($coord[1] > $maxLng) $maxLng = $coord[1];
                            }
                        }
                    }
                }
            }
        }



        $done = 0;

        $markers = array();

        while($done < $amount) {
            $lat = (mt_rand() / mt_getrandmax()) * ($maxLat - $minLat) + $minLat;
            $lng = (mt_rand() / mt_getrandmax()) * ($maxLng - $minLng) + $minLng;

            foreach($shapes as $shape) {
                if ($this->inside($lat, $lng, $shape)) {
                    $marker = new stdClass();
                    //TODO: FUUUCK, I MIXED UP LAT AND LNG... it works this way, but it's confusing!
                    $marker->lng = $lat;
                    $marker->lat = $lng;
                    $markers[] = $marker;
                    $done++;
                    continue;
                }
            }
        }

        $output = new stdClass();
        $output->coordinates = $markers;
        return json_encode($output);
    }

    //Based on https://github.com/substack/point-in-polygon/blob/master/index.js
    private function inside($x, $y, $vs) {

        // ray-casting algorithm based on
        // http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html


        $inside = false;
        for ($i = 0, $j = count($vs) - 1; $i < count($vs); $j = $i++) {
            $xi = $vs[$i][0];
            $yi = $vs[$i][1];

            $xj = $vs[$j][0];
            $yj = $vs[$j][1];

            $intersect = (($yi > $y) != ($yj > $y))
                && ($x < ($xj - $xi) * ($y - $yi) / ($yj - $yi) + $xi);
            if ($intersect) $inside = !$inside;
        }

        return $inside;
    }
}