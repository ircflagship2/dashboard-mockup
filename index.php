<?php
$inc = "fj823qjva89jasjeou8vasjfoa";
require_once('helper.php');

//Grab output
//ob_start();
if (isset($_GET['page']))
    loadPage($_GET['page']);
else
    loadPage('front');
$pageHtmlContent = ob_get_contents();
ob_end_clean();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Flu Dashboard Mockup</title>

    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
    <!-- bootstrap top navbar -->
    <style>
        .navbar-top {
            margin-bottom: 19px;
        }
    </style>
    <?php
    foreach($glob->css as $css) {
        echo '<link href="' . $css . '" rel="stylesheet">' . "\n";
    }
    ?>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>
    <![endif]-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.2/js/bootstrap.min.js"></script>
    <?php
    foreach($glob->js as $js) {
        echo '<script src="' . $js . '" charset="utf-8"></script>' . "\n";
    }
    ?>
</head>

<body>

<!-- Static navbar -->
<div class="navbar navbar-default navbar-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Flu Dashboard Mockup</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li class="<?=isActiveMenu('front')?>"><a href="?page=front">Dashboard</a></li>
                <li class="<?=isActiveMenu('about')?>"><a href="?page=about">About</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Experiments <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li><a href="?page=ukregions">UK Regions</a></li>
                        <li><a href="?page=mapmarkers">Plotting many markers</a></li>
                        <li><a href="?page=timeseries">Time Series with sliding filter</a></li>
                        <li><a href="?page=hexbinning">Hexbins on map</a></li>
                        <li><a href="?page=heatmapuk">UK Heatmap</a></li>
                        <li class="divider"></li>
                        <li><a href="?page=randommarkersservice">Random Marker Webservice</a></li>
                    </ul>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<div class="container">
    <?= $pageHtmlContent ?>
</div> <!-- /container -->

</body>
</html>